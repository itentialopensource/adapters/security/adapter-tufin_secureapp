# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Tufin_secureapp System. The API that was used to build the adapter for Tufin_secureapp is usually available in the report directory of this adapter. The adapter utilizes the Tufin_secureapp API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The Tufin SecureApp adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Tufin SecureApp. With this adapter you have the ability to perform operations such as:

- Applications
- Customers
- Domains
- Tickets

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
