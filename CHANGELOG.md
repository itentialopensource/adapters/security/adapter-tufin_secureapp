
## 0.5.4 [10-15-2024]

* Changes made at 2024.10.14_21:27PM

See merge request itentialopensource/adapters/adapter-tufin_secureapp!15

---

## 0.5.3 [09-19-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-tufin_secureapp!13

---

## 0.5.2 [08-14-2024]

* Changes made at 2024.08.14_19:45PM

See merge request itentialopensource/adapters/adapter-tufin_secureapp!12

---

## 0.5.1 [08-07-2024]

* Changes made at 2024.08.06_21:49PM

See merge request itentialopensource/adapters/adapter-tufin_secureapp!11

---

## 0.5.0 [05-16-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-tufin_secureapp!10

---

## 0.4.3 [03-27-2024]

* Changes made at 2024.03.27_13:53PM

See merge request itentialopensource/adapters/security/adapter-tufin_secureapp!9

---

## 0.4.2 [03-11-2024]

* Changes made at 2024.03.11_16:21PM

See merge request itentialopensource/adapters/security/adapter-tufin_secureapp!8

---

## 0.4.1 [02-27-2024]

* Changes made at 2024.02.27_11:58AM

See merge request itentialopensource/adapters/security/adapter-tufin_secureapp!7

---

## 0.4.0 [12-27-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-tufin_secureapp!6

---
