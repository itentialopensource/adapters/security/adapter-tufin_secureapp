# Tufin_secureapp

Vendor: Tufin
Homepage: https://www.tufin.com/

Product: SecureApp
Product Page: https://forum.tufin.com/support/kc/latest/Content/Suite/sa_overview.htm

## Introduction
We classify Tufin SecureApp into the Security/SASE domain as Tufin SecureApp provide a Security solution.

"SecureApp is an application-centric network security management solution that enables organizations to view their network topology from a functional perspective, and allows them to easily monitor and control communication between applications and services in their network." 
"Applications can be deployed faster, and communication is improved between the application teams and network security teams, while also improving business agility." 

## Why Integrate
The Tufin SecureApp adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Tufin SecureApp. With this adapter you have the ability to perform operations such as:

- Applications
- Customers
- Domains
- Tickets

## Additional Product Documentation
The [SecureChange & SecureApp API](https://forum.tufin.com/support/kc/latest/Content/Suite/RESTAPI/6481.htm)
The [Introduction to Tufin API](https://forum.tufin.com/support/kc/rest-api/Introduction_to_REST_API.pdf)
